# vid Media

## 1. Create the app locally 

```console
$ python3 -m venv venv
$ . venv/bin/activate
$ pip install fastapi uvicorn
```

Try it:

```console
$ uvicorn app.main:app --port 80
```

Save dependencies:

```console
$ pip freeze > requirements.txt
```

## 2. Build the Docker image

```console
$ docker build -t vid-media . 
```

## 3. Run the Docker image

Normal:

```console
$ docker run -p 80:80 vid-media
```

Run in background and give a name:

```console
$ docker run -d --name vid-media -p 80:80 vid-media
```

docker run --name vid-media -p 86:86 -d -v $(pwd):/code vidmedia-image